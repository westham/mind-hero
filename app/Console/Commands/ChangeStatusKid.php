<?php

namespace App\Console\Commands;

use App\Models\Log;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class ChangeStatusKid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logs = Log::where(
            [
                ['created_at', '<=', Carbon::now()->subMinute(300)],
                ['created_at', '>=', Carbon::now()->subMinute(301)],
                ['status', '=', 'emergency']
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->select(['kid_id', 'id'])
            ->get()
            ->unique('kid_id');

        foreach ($logs as $log) {
            $kid = $log->kid;
            $kid->status = 'normal';
            $kid->save();
        }

        return true;
    }
}
