<?php

namespace App\Console\Commands;

use App\Models\PasswordReset;
use Illuminate\Console\Command;

class ClearPasswordResetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:resetting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command clear password reset after one hour if password was not reseted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return PasswordReset::deletePassed1H();
    }
}
