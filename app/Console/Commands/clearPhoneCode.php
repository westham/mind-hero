<?php

namespace App\Console\Commands;

use App\Models\LoginPhone;
use Carbon\Carbon;
use Illuminate\Console\Command;

class clearPhoneCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear old code';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return LoginPhone::where('created_at', '<=', Carbon::now()->subMinute())->delete();
    }
}
