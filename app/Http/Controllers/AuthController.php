<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckPhoneRequest;
use App\Http\Requests\UserRequest;
use App\Http\Traits\AuthTrait;
use App\Models\CountryCodes;
use Auth;
use DB;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    use AuthTrait;

    protected $login = 'email';

    public function login(UserRequest $request)
    {
        if ($request->has('phone') && $request->has('code')) {
            $response = $this->loginPhone($request->phone, $request->code, $request->verification_code, $request->token_device);
            return response($response, 200);
        }

        if ($request->has('email') && $request->has('password')) {
            $response = $this->loginEmail($request->email, $request->password, $this->login);
            return response($response, 200);
        }

        $response = $this->formatResponse('error', 'Parameters missing');
        return response($response, 200);
    }

    public function checkPhone(CheckPhoneRequest $request)
    {
        $response = $this->sendCode($request);

        return response($response, 200);
    }

    public function listPhoneCodes()
    {
        $codes = CountryCodes::getListCodes();

        $response = $this->formatResponse('success', null, $codes);
        return response($response, 200);
    }

    public function logout(Request $request) {
        $user = Auth::user();

        $user->device()->update(['token' => '']);

        $value = $request->bearerToken();

        if ($value) {
            $id = (new Parser())->parse($value)->getHeader('jti');
            DB::table('oauth_access_tokens')->where('id', '=', $id)->update(['revoked' => 1]);
        }

        $response = $this->formatResponse('success', 'You are successfully logged out');
        return response($response, 200);
    }
}
