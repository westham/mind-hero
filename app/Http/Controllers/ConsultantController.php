<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConsultantCreateRequest;
use App\Http\Requests\ConsultantDeleteRequest;
use App\Http\Requests\ConsultantUpdateRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Auth;

class ConsultantController extends Controller
{
    public function index()
    {
        $consultants = User::with('schedules')->whereHas('role', function($q){
            $q->where('name', 'Consultant');
        })->get();

        $response = $this->formatResponse('success', null, $consultants);
        return response($response, 200);
    }

    public function create(ConsultantCreateRequest $request)
    {
        $request['role_id'] = 3;
        $request['password'] = bcrypt($request->password);
        $user = User::create($request->all());

        $response = $this->formatResponse('success', null, $user);
        return response($response, 200);
    }

    public function update(ConsultantUpdateRequest $request)
    {
        $user = User::find($request->consultant_id);
        $user->update($request->all());
        $response = $this->formatResponse('success', null, $user);
        return response($response, 200);
    }

    public function delete(ConsultantDeleteRequest $request)
    {
        $user = User::find($request->consultant_id);
        $user->delete();
        $response = $this->formatResponse('success', null);
        return response($response, 200);
    }
}
