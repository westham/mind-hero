<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContentAddToFavRequest;
use App\Http\Requests\ContentCreateRequest;
use App\Http\Requests\ContentDeleteRequest;
use App\Http\Requests\ContentUpdateRequest;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function index()
    {
        $contents = Content::orderBy('status', 'desc')->orderBy('date', 'asc')->take(config('constants.limit_content'))->get();

        $response = $this->formatResponse('success', null, ContentResource::collection($contents));
        return response($response, 200);
    }

    public function contentsList()
    {
        $data[] = ContentResource::collection(Content::where('status', 'live')->orderBy('date', 'asc')->take(config('constants.limit_content'))->get());

        $data[] = ContentResource::collection(Content::where('status', 'coming')->orderBy('date', 'asc')->take(config('constants.limit_content'))->get());

        $response = $this->formatResponse('success', null, $data);
        return response($response, 200);
    }

    public function liveContents(){
        $contents = ContentResource::collection(Content::with('users')->where('status', 'live')->orderBy('date', 'asc')->get());
        $response = $this->formatResponse('success', null, $contents);
        return response($response, 200);
    }

    public function favouritesList()
    {
        $user = Auth::user();

        $response = $this->formatResponse('success', null, $user->contents);
        return response($response, 200);
    }

    public function search(Request $request)
    {
        $contents = Content::where('name', 'like', "%{$request->keyword}%")->get();

        $response = $this->formatResponse('success', null, $contents);
        return response($response, 200);
    }

    public function create(ContentCreateRequest $request, Content $content)
    {
        $user = Auth::user();
        $credentials = $request->only('name', 'date', 'time', 'image');
        $createdContent = $content->createContent($user, $credentials);
        $response = $this->formatResponse('success', null, new ContentResource($createdContent));
        return response($response, 200);
    }

    public function update(ContentUpdateRequest $request, Content $content)
    {
        $credentials = $request->all();
        $createdContent = $content->updateContent($credentials);
        $response = $this->formatResponse('success', null, new ContentResource($createdContent));
        return response($response, 200);
    }

    public function addToFavourites(ContentAddToFavRequest $request)
    {
        $user = Auth::user();
        if($user->contents->contains($request['content_id'])){
            $checkOwner = $user->contents()->wherePivot('status', 'owner')->find($request['content_id']);
            if(!is_null($checkOwner)){
                $response = $this->formatResponse('error', 'You are owner of this content.');
                return response($response, 200);
            }
            $response = $this->formatResponse('error', 'You already have this content in favourites.');
            return response($response, 200);
        }

        $user->contents()->attach($request->content_id);

        $response = $this->formatResponse('success');
        return response($response, 200);
    }

    public function removeFromFavourites(Request $request)
    {
        $user = Auth::user();

        $user->contents()->detach($request->content_id);

        $response = $this->formatResponse('success');
        return response($response, 200);
    }

    public function delete(ContentDeleteRequest $request){
        Content::find($request->content_id)->delete();
        $response = $this->formatResponse('success', 'Content was successfully deleted.');
        return response($response, 200);
    }

    public function getMy(){
        $user = Auth::user();
        $currWeekContents = $user->contents->filter(function ($item){
            return ($item->date_standart_format > Carbon::now()->startOfWeek()->subDay())
                && ($item->date_standart_format < Carbon::now()->endOfWeek()->subDay());
        });
        $contents = ContentResource::collection($currWeekContents)->sortBy(function ($item){
            return $item->date_standart_format;
        })->groupBy(function ($item) {
            return $item->date_d_format;
        });
        $response = $this->formatResponse('success', null, $contents);
        return response($response, 200);
    }
}
