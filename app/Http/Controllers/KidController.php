<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKidRequest;
use App\Models\ParentKid;
use App\Models\User;
use Auth;
use Gate;
use Illuminate\Http\Request;

class KidController extends Controller
{
    protected $kids = [];

    public function index()
    {
        $kids = Auth::user()->kids;

        $response = $this->formatResponse('success', null, $kids);
        return response($response, 200);
    }

    public function getAll()
    {
        $kids = User::whereHas('role', function ($query){
            $query->where('name', 'Kid');
        })->get();

        $response = $this->formatResponse('success', null, $kids);
        return response($response, 200);
    }

    public function create(CreateKidRequest $request)
    {
        $user = User::getByPhone($request->phone, $request->code)->first();

        if ($user !== null) {
            $response = $this->formatResponse('error', 'User with this phone already exist');
            return response($response, 200);
        }

        $parent = Auth::user();

        $data = $request->all();
        $data['phone'] = ['phone' => $request->phone, 'code' => $request->code];
        $data['role_id'] = 1;

        if ($parent->kids->count() >= 1) {
            $data['discount'] = 50;
        } else {
            $data['discount'] = 0;
        }

        $kid = User::create($data);

        $parent->kids()->attach($kid, ['status' => 'owner']);

        $response = $this->formatResponse('success', null, $kid);
        return response($response, 200);
    }

    public function update(Request $request)
    {
        $kid = User::find($request->kid_id);

        if (Gate::allows('updateKid', $kid)) {
            $kid->update($request->all());

            $response = $this->formatResponse('success', null, $kid);
            return response($response, 200);
        } else {
            $response = $this->formatResponse('error', 'Access denied');
            return response($response, 200);
        }
    }

    public function delete(Request $request)
    {
        $kid = User::find($request->kid_id);

        if (Gate::allows('deleteKid', $kid)) {
            $kid->delete();

            $response = $this->formatResponse('success', null, $kid);
            return response($response, 200);
        } else {
            $response = $this->formatResponse('error', 'Access denied');
            return response($response, 200);
        }
    }

    public function changeAccess(Request $request)
    {
        $kid = User::find($request->kid_id);

        $kid->access = $request->access;
        $kid->save();

        $response = $this->formatResponse('success', null, $kid);
        return response($response, 200);
    }

    public function getPaidKids(){
        $paidKids = User::getPaidKids();
        $response = $this->formatResponse('success', null, $paidKids);
        return response($response, 200);
    }

}
