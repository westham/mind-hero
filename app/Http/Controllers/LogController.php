<?php

namespace App\Http\Controllers;

use App\Traits\PushNotificationTrait;
use App\Models\Log;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class LogController extends Controller
{
    use PushNotificationTrait;

    public function changeStatus()
    {
//        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/../../../firebase-config.json');
//
//        $firebase = (new Factory)
//
//            ->withServiceAccount($serviceAccount)
//
//            ->withDatabaseUri('https://mind-hero-96b57.firebaseio.com/')
//
//            ->create();
//
//        $database = $firebase->getDatabase();
//
//        $newPost = $database
//
//            ->getReference('logs/1')
//
//            ->getChild('0/description')->set('Changed post title');

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/../../../firebase-config.json');

        $firebase = (new Factory)

            ->withServiceAccount($serviceAccount)

            ->withDatabaseUri('https://mind-hero-96b57.firebaseio.com/')

            ->create();

        $database = $firebase->getDatabase();

        $newPost = $database->getReference('logs');

//            ->getChild('0/description')->set('Changed post title');

        return $newPost;
    }

    public function index(Request $request)
    {
        $parent = Auth::user();

        if($parent->role->name == 'Parent'){
            $logs = $parent->kids()->find($request->kid_id)->kidLogs;
            $response = $this->formatResponse('success', null, $logs);
        } else {
            $response = $this->formatResponse('error', 'You are not a parent');
        }
        return response($response, 200);
    }

    public function emergencyLogs()
    {
        $logs = Log::with(['kid', 'consultant'])->where('status', 'emergency')->get();

        $response = $this->formatResponse('success', null, $logs);
        return response($response, 200);
    }

    public function create(Request $request)
    {
        $consultant = Auth::user();

        $log = $consultant->consultantLogs()->create($request->all());

        $kid = $log->kid;

        if ($request->status == 'emergency') {

            $kid->status = 'emergency';
            $kid->save();
        }

        $consultant = $log->consultant;

        $parents = $log->kid->parents;

        $to = [];

        foreach ($parents as $parent) {
            if ($parent->device && $parent->device->token !== null) {
                $to[] = $parent->device->token;
            }
        }

        if(!empty($to)){
            if ($request->status == 'emergency') {
                $this->sendNotification($to, 'Mind Hero', 'You have received a new emergency comment from '.$consultant->name.' about '.$kid->name.' .', ['type' => 'log', 'status' => 'emergency', 'message' => 'You have received a new emergency comment from '.$consultant->name.' about '.$kid->name.' .', 'kid_id' => $kid->id]);
            }

            if ($request->status == 'normal') {
                $this->sendNotification($to, 'Mind Hero', 'You have received a new comment from '.$consultant->name.' about '.$kid->name.' .', ['type' => 'log', 'status' => 'normal', 'message' => 'You have received a new comment from '.$consultant->name.' about '.$kid->name.' .', 'kid_id' => $kid->id]);
            }
        }

        $response = $this->formatResponse('success', null, $log);
        return response($response, 200);
    }

    public function delete(Request $request)
    {
        Log::find($request->log_id)->delete();

        $response = $this->formatResponse('success', 'Log deleted');
        return response($response, 200);
    }
}
