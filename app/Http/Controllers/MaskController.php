<?php

namespace App\Http\Controllers;

use App\Http\Requests\MaskCreateRequest;
use App\Http\Requests\MaskDeleteRequest;
use App\Http\Requests\MaskUpdateRequest;
use App\Http\Resources\MasksResource;
use App\Mask;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;

class MaskController extends Controller
{
    use ImageTrait;

    public function create(MaskCreateRequest $request){
        $credentials = $request->only('name', 'file', 'y_offset');
        $savePath = Config::get('constants.image_folder.masks.save_path');
        $resizePath = Config::get('constants.image_folder.masks.resize_path');
        $image = $this->saveImage($credentials['file'], $savePath);
        Image::make($resizePath.$image)->resize(480, 480)->save();
        $credentials['file'] = $image;
        $mask = Mask::create($credentials);
        $response = $this->formatResponse('success', null, new MasksResource($mask));
        return response($response, 200);
    }

    public function getAll(){
        $masks = Mask::all();
        $response = $this->formatResponse('success', null, MasksResource::collection($masks));
        return response($response, 200);
    }

    public function update(MaskUpdateRequest $request){
        $credentials = $request->only('name', 'y_offset');
        $mask = Mask::find($request->mask_id);
        $mask->update($credentials);
        $response = $this->formatResponse('success', null, new MasksResource($mask));
        return response($response, 200);
    }

    public function delete(MaskDeleteRequest $request){
        $credentials = $request->only('masks_id');
        $savePath = Config::get('constants.image_folder.masks.save_path');
        Mask::whereIn('id', $credentials['masks_id'])->get()->map(function ($item) use($savePath){
            $this->deleteImage($item->file, $savePath);
            $item->delete();
        });
        $response = $this->formatResponse('success', null);
        return response($response, 200);
    }

}
