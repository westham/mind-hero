<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnotherParentDeleteRequest;
use App\Http\Requests\ParentsListByKidRequest;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ParentController extends Controller
{
    public function listFollowers(Request $request)
    {
        $kid = User::find($request->kid_id);

        $parents = $kid->parents()->wherePivot('status', 'follower')->get();

        $response = $this->formatResponse('success', null, $parents);
        return response($response, 200);
    }

    public function getAll()
    {
        $parents = User::with('kids')->whereHas('role', function ($query){
            $query->where('name', 'Parent');
        })->get();

        $response = $this->formatResponse('success', null, $parents);
        return response($response, 200);
    }

    public function checkStatusParent(Request $request)
    {
        $parent = Auth::user();

        $kid = $parent->kids()->wherePivot('status', 'owner')->find($request->kid_id);

        if ($kid !== null) {
            $response = $this->formatResponse('success', null, true);
            return response($response, 200);
        } else {
            $response = $this->formatResponse('error', null, false);
            return response($response, 200);
        }
    }

    public function addAnotherParent(Request $request)
    {
        $user = User::getByPhone($request->phone, $request->code)->first();

        if ($user !== null) {
            $response = $this->formatResponse('error', 'User with this phone already exist');
            return response($response, 200);
        }

//        if ($request->type == 'owner') {
//            if ($kid->parents->count() >= 2) {
//                $response = $this->formatResponse('error', 'It is impossible to extract more than two parents');
//                return response($response, 200);
//            }
//        }

        $parent = User::create([
            'phone' => [
                'phone' => $request->phone,
                'code' => $request->code
            ],
            'role_id' => 2
        ]);

        switch ($request->type) {
            case "follower":
                $kid = User::find($request->kid_id);

                $kid->parents()->attach($parent->id, ['status' => 'follower']);

                //Twilio::message($parent->phone(), 'You are invited to the application Mind Hero. Link to download: Play Market - https://play-market.com, App Store - https://app-store.com');

                $response = $this->formatResponse('success', null, $parent);
                return response($response, 200);

                break;

            case "owner":
                $kids = Auth::user()->kids;

                $parent->kids()->attach($kids->pluck('id'), ['status' => 'owner']);

                //Twilio::message($parent->phone(), 'You are invited to the application Mind Hero. Link to download: Play Market - https://play-market.com, App Store - https://app-store.com');

                $response = $this->formatResponse('success', null, $parent);
                return response($response, 200);

                break;

            default:
                $parent->delete();

                $response = $this->formatResponse('error', 'Not changed type parent');
                return response($response, 200);
        }
    }

    public function deleteAnotherParent(AnotherParentDeleteRequest $request)
    {
        $currentUser = Auth::user();
        $kid = User::find($request->kid_id);
        if(!$currentUser->kids->contains($kid)){
            $response = $this->formatResponse('error', 'This kid does not belong you');
            return response($response, 200);
        }
        $kid->parents()->detach($request->follower_id);
        $response = $this->formatResponse('success', 'Follower was successfully removed.');
        return response($response, 200);
    }

    public function listParents(ParentsListByKidRequest $request){
        $parents = User::find($request->kid_id)->parents;
        $response = $this->formatResponse('success', null, $parents);
        return response($response, 200);
    }
}
