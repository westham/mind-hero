<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordForgotRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Models\User;
use App\Models\PasswordReset;
use App\Traits\EmailTrait;
use Illuminate\Contracts\Mail\MailQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PasswordController extends Controller
{
    use EmailTrait;

    protected $mailer;

    public function __construct(MailQueue $mailer)
    {
        $this->mailer = $mailer;
    }


    public function forgot(PasswordForgotRequest $request){
        $email = $request->email;
        $user = User::getByEmail($email)->first();
        if(PasswordReset::checkByEmail($user->email)){
            $response = $this->formatResponse('error', 'Mail for resetting password was already sended.');
            return response($response, 200);
        }
        $this->sendMail($user);
        $response = $this->formatResponse('success', 'Mail was successfully sended.');
        return response($response, 200);
    }

    public function reset(PasswordResetRequest $request){
        $credentials = $request->only('email', 'key', 'password');
        if(PasswordReset::checkByEmailAndKey($credentials['email'], $credentials['key'])){
            User::getByEmail($credentials['email'])->first()->update(['password' => bcrypt($credentials['password'])]);
            PasswordReset::deleteByEmail($credentials['email']);
            $response = $this->formatResponse('success', 'Password was successfully reset.');
            return response($response, 200);
        }
        $response = $this->formatResponse('error', 'This key or email is wrong.');
        return response($response, 200);
    }
}
