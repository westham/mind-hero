<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentCreateRequest;
use App\Models\Payment;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        $payments->each(function ($item){
            $item->number_kids = $item->tariff->number_kids;
        });
        $response = $this->formatResponse('success', null, $payments);
        return response($response, 200);
    }

    public function create(PaymentCreateRequest $request, Payment $payment)
    {
        $user = Auth::user();
        $credentials = $request->all();
        $payment = $payment->createPayment($user, $credentials);
        return $payment;
    }
}
