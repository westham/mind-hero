<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReasonCreateRequest;
use App\Http\Requests\ReasonDeleteRequest;
use App\Http\Requests\ReasonUpdateRequest;
use App\Models\Reason;
use Illuminate\Http\Request;

class ReasonController extends Controller
{
    public function index()
    {
        $all = Reason::all();
        return response($all);
    }

    public function store(ReasonCreateRequest $request){

        $report = Reason::create($request->all());

        $response = $this->formatResponse('success', 'Reason was successfully created.', $report);
        return response($response, 200);
    }

    public function update(ReasonUpdateRequest $request){
        $credentials = $request->only(['name', 'description']);
        $report = Reason::find($request->reason_id);
        $report->update(['name' => $credentials['name'], 'description' => $credentials['description']]);
        $response = $this->formatResponse('success', 'Reason was successfully updated.', $report);
        return response($response, 200);
    }

    public function delete(ReasonDeleteRequest $request){
        $credentials = $request->only('ids');
        Reason::whereIn('id', $credentials['ids'])->delete();
        $response = $this->formatResponse('success', 'Reason was successfully deleted.');
        return response($response, 200);
    }

}
