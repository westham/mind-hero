<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportSendRequest;
use App\Http\Resources\ReportResource;
use App\Models\Reason;
use App\Models\Report;
use Auth;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function sendReport(ReportSendRequest $request, Report $report){
        $currentUser = Auth::user();
        $credentials = $request->all();
        if($request->has('own_reason')){
            $name = 'own_reason_'.$currentUser->name.'_'.time();
            $reason = Reason::create(['name' => $name, 'description' => $request->own_reason]);
            $credentials['reason_id'] = $reason->id;
        }
        $sendedReport = $report->send($currentUser, $credentials);
        $response = $this->formatResponse('success', 'Report was successfully sended.',  new ReportResource($sendedReport));
        return response($response, 200);    }
}
