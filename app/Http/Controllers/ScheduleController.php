<?php
namespace App\Http\Controllers;

use App\Http\Requests\ScheduleApproveRequest;
use App\Http\Requests\ScheduleCreateRequest;
use App\Http\Requests\ScheduleDeleteRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Http\Requests\ScheduleVisibleRequest;
use App\Http\Resources\ScheduleResource;
use App\Models\Schedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{
    public function create(ScheduleCreateRequest $request, Schedule $schedule){
        $currentUser = Auth::user();
        $credentials = $request->only('time_from', 'time_to');
        $createdSchedule = $schedule->createSchedule($currentUser, $credentials);
        $response = $this->formatResponse('success', null, new ScheduleResource($createdSchedule));
        return response($response, 200);
    }

    public function update(ScheduleUpdateRequest $request){
        $credentials = $request->all();
        $updatedSchedule = Schedule::find($request->schedule_id)->updateSchedule($credentials);
        $response = $this->formatResponse('success', null, new ScheduleResource($updatedSchedule));
        return response($response, 200);
    }

    public function delete(ScheduleDeleteRequest $request){
        $user = Auth::user();
        if($user->role->name == 'Consultant'){
            if(!$user->schedules->contains($request->schedule_id)){
                $response = $this->formatResponse('error', 'This is not your schedule.');
                return response($response, 200);
            }
        }
        Schedule::find($request->schedule_id)->delete();
        $response = $this->formatResponse('success', 'Schedule was successfully deleted.');
        return response($response, 200);
    }

    public function getMy(){
        $user = Auth::user();
        $schedules = $user->schedules;
        $response = $this->formatResponse('success', null, ScheduleResource::collection($schedules));
        return response($response, 200);
    }

    public function getByConsultant($id){
        $user = User::find($id);
        $schedules = $user->schedules->where('visible', true);
        $response = $this->formatResponse('success', null, ScheduleResource::collection($schedules));
        return response($response, 200);
    }

    public function approve(ScheduleApproveRequest $request){
        $scheduleIds = $request->schedule_id;
        Schedule::whereIn('id', $scheduleIds)->update(['status' => true]);
        $response = $this->formatResponse('success', 'Schedules were successfully approved.');
        return response($response, 200);
    }

    public function visible(ScheduleVisibleRequest $request){
        $scheduleIds = $request->schedule_id;
        Schedule::whereIn('id', $scheduleIds)->update(['visible' => true]);
        $response = $this->formatResponse('success', 'Schedules were successfully made visible.');
        return response($response, 200);
    }

    public function getAll(){
        $schedules = Schedule::with('user')->get();
        $response = $this->formatResponse('success', null, ScheduleResource::collection($schedules));
        return response($response, 200);
    }

    public function getMy2Weeks(){
        $user = Auth::user();
        $currWeekSchedules = $user->schedules()->currentWeek()->where('status', true);
        $nextWeekSchedules = $user->schedules()->nextWeek()->where('status', true);
        $schedules['current_week'] = ScheduleResource::collection($currWeekSchedules)->sortBy('time_from')->groupBy(function ($item) {
            return Carbon::parse($item['time_from'])->format('D');
        });
        $schedules['next_week'] = ScheduleResource::collection($nextWeekSchedules)->sortBy('time_from')->groupBy(function ($item) {
            return Carbon::parse($item['time_from'])->format('D');
        });
        $response = $this->formatResponse('success', null, $schedules);
        return response($response, 200);
    }

    public function getAll2Weeks(){
        $currWeekSchedules = Schedule::currentWeek();
        $nextWeekSchedules = Schedule::nextWeek();
        $schedules['current_week'] = ScheduleResource::collection($currWeekSchedules)->sortBy('time_from')->groupBy(function ($item) {
            return Carbon::parse($item['time_from'])->format('D');
        });
        $schedules['next_week'] = ScheduleResource::collection($nextWeekSchedules)->sortBy('time_from')->groupBy(function ($item) {
            return Carbon::parse($item['time_from'])->format('D');
        });
        $response = $this->formatResponse('success', null, $schedules);
        return response($response, 200);
    }
}
