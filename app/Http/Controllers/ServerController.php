<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServerCreateRequest;
use App\Http\Requests\ServerUpdateRequest;
use App\Http\Resources\ServerResource;
use App\Models\Server;

class ServerController extends Controller
{
    public function getAll(){
        $servers = Server::all();
        $response = $this->formatResponse('success', null, ServerResource::collection($servers));
        return response($response, 200);
    }

    public function create(ServerCreateRequest $request){
        $credentials = $request->all();
        $server = Server::create($credentials);
        $response = $this->formatResponse('success', null, new ServerResource($server));
        return response($response, 200);
    }

    public function update(ServerUpdateRequest $request){
        $credentials = $request->except('server_id');
        $server = Server::find($request->server_id);
        $server->update($credentials);
        $response = $this->formatResponse('success', null, new ServerResource($server));
        return response($response, 200);
    }
}
