<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaticInfoCreateRequest;
use App\Http\Requests\StaticInfoUpdateRequest;
use App\Http\Resources\StaticInfoResource;
use App\Models\StaticInformation;
use Illuminate\Http\Request;

class StaticInformationController extends Controller
{
    public function index(Request $request)
    {
        $static_informations = StaticInformation::where('type', $request->type)->get();

        $response = $this->formatResponse('success', null, $static_informations);
        return response($response, 200);
    }

    public function create(StaticInfoCreateRequest $request, StaticInformation $staticInformation)
    {
        $credentials = $request->all();
        $createdStaticInfo = $staticInformation->createStaticInfo($credentials);
        $response = $this->formatResponse('success', null, new StaticInfoResource($createdStaticInfo));
        return response($response, 200);
    }

    public function update(StaticInfoUpdateRequest $request, StaticInformation $staticInformation)
    {
        $credentials = $request->all();
        $updatedStaticInfo = $staticInformation->updateStaticInfo($credentials);
        $response = $this->formatResponse('success', null, new StaticInfoResource($updatedStaticInfo));
        return response($response, 200);
    }

    public function delete(Request $request)
    {
        $static_information = StaticInformation::find($request->static_information_id);

        $static_information->delete();

        $response = $this->formatResponse('success', null, $static_information);
        return response($response, 200);
    }
}
