<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatisticsAddConvRequest;
use App\Http\Resources\StatisticsResource;
use App\Models\Statistics;
use App\Traits\TimeTrait;
use Auth;


class StatisticsController extends Controller
{
    use TimeTrait;

    public function add(StatisticsAddConvRequest $request, Statistics $statistics){
        $currentUser = Auth::user();
        $credentials = $request->only('type', 'add_info');
        if($credentials['type'] == 'chat' && Statistics::check($credentials['type'], $credentials['add_info']['interlocutor_id'])){
            $response = $this->formatResponse('success', 'This statistic is already exist.');
            return response($response, 200);
        }
        $createdStatistic = $statistics->createStatistic($currentUser, $credentials);
        $response = $this->formatResponse('success', 'Statistic was successfully created', new StatisticsResource($createdStatistic));
        return response($response, 200);
    }

    public function getMyChatTodayStatistic(){
        $currentUser = Auth::user();
        $chatCount = $currentUser->statistics()->getChatToday()->count();
        $statistics = ['chats' => $chatCount];
        $response = $this->formatResponse('success', null, $statistics);
        return response($response, 200);
    }

    public function getMyCallTodayStatistic(){
        $currentUser = Auth::user();
        $callStatistics = $currentUser->statistics()->getCallToday();
        $callCount = $callStatistics->count();
        $callTime = $this->sumTime($callStatistics->pluck('add_info')->map(function ($item){
            return json_decode($item);
        })->pluck('time')->toArray());
        $statistics = ['calls' => $callCount, 'time' => $callTime];
        $response = $this->formatResponse('success', null, $statistics);
        return response($response, 200);
    }

    public function getMyWeeklyStatistic(){
        $currentUser = Auth::user();
        $chatCount = $currentUser->statistics()->getChatWeekly()->count();
        $callStatistics = $currentUser->statistics()->getCallWeekly();
        $callCount = $callStatistics->count();
        $callTime = $this->sumTime($callStatistics->pluck('add_info')->map(function ($item){
            return json_decode($item);
        })->pluck('time')->toArray());
        $statistics = ['calls' => $callCount, 'time' => $callTime, 'chats' => $chatCount];
        $response = $this->formatResponse('success', null, $statistics);
        return response($response, 200);
    }
}
