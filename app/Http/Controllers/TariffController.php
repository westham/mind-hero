<?php

namespace App\Http\Controllers;

use App\Http\Requests\TariffRequest;
use App\Models\Tariff;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    public function index(Request $request)
    {
        $tariffs = Tariff::where('number_kids', $request->number_kids)->get();

        $response = $this->formatResponse('success', null, $tariffs);
        return response($response, 200);
    }

    public function create (TariffRequest $request)
    {
        $tariff = Tariff::create($request->all());

        $response = $this->formatResponse('success', null, $tariff);
        return response($response, 200);
    }

    public function update(Request $request)
    {
        $tariff = Tariff::find($request->tariff_id);

        $tariff->update($request->all());

        $response = $this->formatResponse('success', null, $tariff);
        return response($response, 200);
    }

    public function delete(Request $request)
    {
        $tariff = Tariff::find($request->tariff_id);

        $tariff->delete();

        $response = $this->formatResponse('success', null, $tariff);
        return response($response, 200);
    }
}
