<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function show()
    {
        $response = $this->formatResponse('success', null, Auth::user());
        return response($response, 200);
    }

    public function update(UserUpdateRequest $request)
    {
        $user = Auth::user();

        $user->update($request->all());

        $response = $this->formatResponse('success', null, $user);
        return response($response, 200);
    }

    public function getAll(){
        $users = User::with(['parents' => function($query){
            $query->wherePivot('status', 'follower');
        }])->get();
        $response = $this->formatResponse('success', null, $users);
        return response($response, 200);
    }
}
