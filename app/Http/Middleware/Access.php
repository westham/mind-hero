<?php

namespace App\Http\Middleware;

use App\Http\Traits\FormatResponse;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Closure;

class Access
{
    use FormatResponse;

    const KID = 'Kid';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUser = Auth::user();
        if ($currentUser->role->name == self::KID) {
            if($currentUser->access){
                if($currentUser->payments){
                    if($currentUser->payments->contains('status', true)){
                        $payment = $currentUser->payments->where('status', true)->first();
                        $period = $payment->tariff->period;
                        if($payment->updated_at > Carbon::now()->subDays($period)->timestamp){
                            return $next($request);
                        }
                        $payment->status = false;
                        $payment->save();
                        $currentUser->payment = false;
                        $currentUser->access = false;
                        $currentUser->save();
                    }
                }
            }
            return response()->json(['status' => 'error', 'message' => 'payment error'], 200);
        } else {
            return $next($request);
        }
    }
}
