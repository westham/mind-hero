<?php

namespace App\Http\Middleware;

use App\Http\Traits\FormatResponse;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Closure;

class LoginAccessForKid
{
    use FormatResponse;

    const KID = 'Kid';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userQuery = User::getByPhone($request->phone, $request->code);
        if($userQuery->exists()){
            $user = $userQuery->get()->first();
            if($user->role->name == self::KID){
                $payment = $user->payments->where('status', true)->first();
                if ($payment){
                    $period = $payment->tariff->period;
                    if($user->access && $payment->updated_at > Carbon::now()->subDays($period)->timestamp){
                        return $next($request);
                    }
                }
                return response()->json(['status' => 'error', 'message' => 'payment error'], 200);
            }
        }
        return $next($request);
    }
}
