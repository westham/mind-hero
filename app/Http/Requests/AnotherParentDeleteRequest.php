<?php

namespace App\Http\Requests;

class AnotherParentDeleteRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'follower_id' => 'required|numeric|exists:users,id',
            'kid_id' => 'required_with:follower_id|numeric|exists:users,id|kid|follower:follower_id'
        ];
    }
}
