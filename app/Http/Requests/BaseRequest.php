<?php
/**
 * Created by PhpStorm.
 * User: westham
 * Date: 02.07.18
 * Time: 16:55
 */

namespace App\Http\Requests;

use App\Http\Traits\FormatResponse;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\ValidationException;

class BaseRequest extends FormRequest
{
    use FormatResponse;

    protected function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator, $this->buildResponse($validator)));
    }

    protected function buildResponse($validator)
    {
        $response = $this->formatResponse('error', $validator->errors()->first());
        return response($response, 200);
    }
}