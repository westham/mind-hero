<?php

namespace App\Http\Requests;

class ConsultantCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha|min:3',
            'email' => array('required', 'email', 'unique:users', 'regex:/^[+_.a-zA-Z0-9\'-]*\@+[_a-zA-Z0-9\'-\.]*\.+[_a-zA-Z0-9\'-\.]*$/'),
            'password' => 'required|string|min:6|max:14',
            'id_number' => 'required|numeric|unique:users'
        ];
    }
}
