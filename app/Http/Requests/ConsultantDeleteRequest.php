<?php

namespace App\Http\Requests;

class ConsultantDeleteRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'consultant_id' => 'required|numeric|exists:users,id|consultant'
        ];
    }
}
