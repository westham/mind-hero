<?php

namespace App\Http\Requests;

class ConsultantUpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'consultant_id' => 'filled|numeric|exists:users,id|consultant',
            'name' => 'filled|alpha|min:3',
            'email' => array('filled', 'email', 'unique:users,email,'.$this->consultant_id, 'regex:/^[+_.a-zA-Z0-9\'-]*\@+[_a-zA-Z0-9\'-\.]*\.+[_a-zA-Z0-9\'-\.]*$/'),
            'password' => 'filled|string|min:6|max:14',
            'id_number' => 'filled|numeric|unique:users,id_number,'.$this->consultant_id
        ];
    }
}
