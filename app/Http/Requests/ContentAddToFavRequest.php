<?php

namespace App\Http\Requests;

class ContentAddToFavRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content_id' => 'required|numeric|exists:contents,id',
            'status' => 'required|in:follower'
        ];
    }
}
