<?php

namespace App\Http\Requests;

class ContentUpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'date' => 'required|date_format:d.m.Y',
            'time' => 'required_with:date|date_format:H:i',
            'image' => 'filled|image',
            'content_id' => 'required|numeric|exists:contents,id|my_content'
        ];
    }
}
