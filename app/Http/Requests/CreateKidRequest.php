<?php

namespace App\Http\Requests;

class CreateKidRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'grade' => 'required',
            'id_number' => 'required|numeric|unique:users',
            'birth_date' => 'required',
        ];
    }
}
