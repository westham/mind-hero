<?php

namespace App\Http\Requests;

class PaymentCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'priceAsDecimal' => 'required|numeric',
            'title' => 'required|string',
            'currency' => 'required|string',
            'price' => 'required|string',
            'description' => 'required|string|max:255',
            'kid_id' => 'required|array',
            'kid_id.*' => 'numeric|distinct|exists:users,id',
            'tariff_id' => 'required|exists:tariffs,id'
        ];
    }
}
