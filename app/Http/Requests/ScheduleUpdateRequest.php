<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class ScheduleUpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'schedule_id' => 'required|exists:schedules,id,user_id,'.$this->user()->id,
            'time_from' => 'required|date_format:Y-m-d\TH:i:s',
            'time_to' => 'required_with:time_from|date_format:Y-m-d\TH:i:s'
        ];
    }
}
