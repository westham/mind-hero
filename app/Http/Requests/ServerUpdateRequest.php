<?php

namespace App\Http\Requests;

class ServerUpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'server_id' => 'required|numeric|exists:servers,id',
            'url' => 'required|string|unique:servers,url,'.$this->server_id,
            'credential' => 'filled|string',
            'username' => 'filled|string'
        ];
    }
}
