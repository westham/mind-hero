<?php

namespace App\Http\Requests;

class StaticInfoUpdateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'static_info_id' => 'required|numeric|exists:static_informations,id',
            'title' => 'required|string',
            'content' => 'required|string',
            'type' => 'required|string|unique:static_informations,type,'.$this->static_info_id,
            'image' => 'filled|image'
        ];
    }
}
