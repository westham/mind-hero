<?php

namespace App\Http\Requests;

class StatisticsAddConvRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:chat,call',
            'add_info' => 'required|array',
            'add_info.interlocutor_id' => 'required|numeric|exists:users,id|notMySelf',
            'add_info.time' => 'required_if:type,call|date_format:H:i'
        ];
    }
}
