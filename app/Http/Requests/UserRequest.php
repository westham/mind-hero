<?php

namespace App\Http\Requests;

class UserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => array('email', 'regex:/^[+_.a-zA-Z0-9\'-]*\@+[_a-zA-Z0-9\'-\.]*\.+[_a-zA-Z0-9\'-\.]*$/'),
            'password' => 'filled',
            'phone' => 'filled|numeric',
            'code' => 'filled|numeric',
            'verification_code' => 'filled|digits:4'
        ];
    }
}
