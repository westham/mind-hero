<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Config;

class ContentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $getPath = Config::get('constants.image_folder.image.get_path');
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date' => $this->date,
            'time' => $this->time,
            'image' => url($getPath.$this->image),
            'status' => $this->status,
            'favorite_status' => $this->favorite_status,
            'psychology' => $this->users()->wherePivot('status', 'owner')->get(),
            'current_users' => $this->users()->wherePivot('status', 'follower')->count(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
