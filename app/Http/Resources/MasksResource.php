<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Config;

class MasksResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $getPath = Config::get('constants.image_folder.masks.get_path');
        $file = url($getPath.$this->file);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'file' => $file,
            'y_offset' => $this->y_offset
        ];
    }
}
