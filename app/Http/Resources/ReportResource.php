<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ReportResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'kid' => $this->kid_name,
            'consultant' => $this->consultant_name,
            'reason' => $this->reason
        ];
    }
}
