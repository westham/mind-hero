<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ServerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return collect([
            'id' => $this->id,
            'url' => $this->url,
            'username' => $this->username,
            'credential' => $this->credential
        ])->filter();
    }
}
