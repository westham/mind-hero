<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 29.08.2018
 * Time: 16:46
 */

namespace App\Http\Rules;


use App\Models\Schedule;

class AvailableRule
{
    public function available($attribute, $value, $parameters, $validator)
    {
        $schedules = Schedule::trueStatus($value);
        if($schedules){
            $schedules = implode(',', $schedules);
            return $validator->errors()->add($attribute, "These schedules ($schedules) are already approved.");
        }
        return true;
    }
}