<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 31.08.2018
 * Time: 13:28
 */

namespace App\Http\Rules;


use App\Models\User;

class ConsultantRule
{
    public function consultant($attribute, $value, $parameters, $validator)
    {
        $user = User::find($value);
        if ($user && $user->role->name == 'Consultant') {
            return true;
        }
        return $validator->errors()->add($attribute, 'This consultant does not exist.');
    }
}