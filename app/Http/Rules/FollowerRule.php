<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 29.08.2018
 * Time: 12:15
 */

namespace App\Http\Rules;


use App\Models\User;

class FollowerRule
{
    public function follower($attribute, $value, $parameters, $validator)
    {
        $follower_id = array_get($validator->getData(), $parameters[0], null);
        $follower = User::find($follower_id);
        if($follower->kids && $follower->kids->contains($value)){
            if ($follower->kids()->wherePivot('status', 'follower')->get()->contains($value)){
                return true;
            }
            return $validator->errors()->add($attribute, 'This is not follower.');
        }
        return $validator->errors()->add($attribute, 'This follower does not belong to this kid.');
    }
}