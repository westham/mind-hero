<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 29.08.2018
 * Time: 12:02
 */

namespace App\Http\Rules;


use App\Models\User;

class KidRule
{
    public function kid($attribute, $value, $parameters, $validator)
    {
        $userRole = User::find($value);
        if ($userRole && $userRole->role->name !== 'Kid') {
            return $validator->errors()->add($attribute, 'This kid does not exist.');
        }
        return true;
    }
}