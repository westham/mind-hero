<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 02.09.2018
 * Time: 18:34
 */

namespace App\Http\Rules;


use Auth;

class MyContentRule
{
    public function myContent($attribute, $value, $parameters, $validator)
    {
        $currentUser = Auth::user();
        if ($currentUser->role->name == 'Admin'){
            return true;
        }
        if($currentUser->contents){
            if ($currentUser->contents->contains($value)) {
                return true;
            }
        }
        return $validator->errors()->add($attribute, 'This content does not belong you.');
    }
}