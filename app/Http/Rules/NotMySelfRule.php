<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 28.08.2018
 * Time: 14:36
 */

namespace App\Http\Rules;


use Auth;

class NotMySelfRule
{
    public function notMySelf($attribute, $value, $parameters, $validator)
    {
        $currentUser = Auth::user();
        if ($currentUser->id == $value) {
            return $validator->errors()->add($attribute, 'You cannot be interlocutor.');
        }
        return true;
    }
}