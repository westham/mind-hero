<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 29.08.2018
 * Time: 16:33
 */

namespace App\Http\Rules;


use App\Models\Schedule;

class VisibleRule
{
    public function visible($attribute, $value, $parameters, $validator)
    {
        $schedules = Schedule::notVisible($value);
        if($schedules){
            $schedules = implode(',', $schedules);
            return $validator->errors()->add($attribute, "These schedules ($schedules) are not visible.");
        }
        return true;
    }
}