<?php

namespace App\Models;

use App\Traits\ImageTrait;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Content extends Model
{
    use ImageTrait;

    protected $fillable = ['name', 'date', 'time', 'status', 'image'];

    protected $appends = ['favorite_status'];

    /** RELATIONS **/

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /** RELATIONS **/


    /** SCOPES **/


    /** SCOPES **/


    /** MUTATORS **/

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date)->format('Y-m-d');
    }

    /** MUTATORS **/


    /** ACCESSORS **/

    public function getDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d.m.Y');
    }

    public function getDateStandartFormatAttribute()
    {
        return Carbon::parse($this->date.$this->time)->format('Y-m-d H:i:s');
    }

    public function getDateDFormatAttribute()
    {
        return Carbon::parse($this->date.$this->time)->format('D');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getFavoriteStatusAttribute()
    {
        $user = Auth::user();

        $follower = $user->contents()->wherePivot('status', 'follower')->find($this->id);

        if ($follower !== null) {
            return 'follower';
        }

        $owner = $user->contents()->wherePivot('status', 'owner')->find($this->id);

        if ($owner !== null) {
            return 'owner';
        }

       return null;
    }

    /** ACCESSORS **/

    public function createContent($user, $data){
        try {
            $content = new Content();
            $content->name = $data['name'];
            $content->date = $data['date'];
            $content->time = $data['time'];
            $content->status = 'coming';
            if(array_key_exists('image', $data)){
                $savePath = Config::get('constants.image_folder.image.save_path');
                $content->image = $this->saveImage($data['image'], $savePath);
            }
            $content->save();
            $user->contents()->attach($content->id, ['status' => 'owner']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $content;
    }

    public function updateContent($data){
        try {
            $content = Content::find($data['content_id']);
            $content->name = $data['name'];
            $content->date = $data['date'];
            $content->time = $data['time'];
            if(array_key_exists('image', $data)){
                $savePath = Config::get('constants.image_folder.image.save_path');
                if($content->image){
                    $this->deleteImage($content->image, $savePath);
                }
                $content->image = $this->saveImage($data['image'], $savePath);
            }
            $content->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $content;
    }
}
