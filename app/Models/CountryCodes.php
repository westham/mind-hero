<?php

namespace App\Models;

use App\Http\Traits\FormatResponse;
use Illuminate\Database\Eloquent\Model;

class CountryCodes extends Model
{
    use FormatResponse;

    public static function getListCodes()
    {
        $codes = CountryCodes::get(['name', 'code']);

        return $codes;
    }
}
