<?php

namespace App\Models;

use Carbon\Carbon;
use Firebase\FirebaseLib;
use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

class Log extends Model
{
    use SyncsWithFirebase;

    protected $fillable = ['content', 'status', 'kid_id', 'consultant_id'];

    public function consultant()
    {
        return $this->belongsTo(User::class, 'consultant_id', 'id');
    }

    public function kid()
    {
        return $this->belongsTo(User::class, 'kid_id', 'id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function saveToFirebase($mode)
    {
        if (is_null($this->firebaseClient)) {
            $this->firebaseClient = new FirebaseLib(config('services.firebase.database_url'), config('services.firebase.secret'));
        }
        $path = $this->getTable() . '/'.$this->fresh()->kid_id.'/' . $this->getKey();

        if ($mode === 'set' && $fresh = $this->fresh()) {
            $this->firebaseClient->set($path, $fresh->toArray());
        } elseif ($mode === 'update' && $fresh = $this->fresh()) {
            $this->firebaseClient->update($path, $fresh->toArray());
        } elseif ($mode === 'delete') {
            $this->firebaseClient->delete($path);
        }
    }
}
