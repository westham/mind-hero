<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LoginPhone extends Model
{
    protected $fillable = ['code', 'user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function scopeExistCode($query, $user_id, $code)
    {
        return $query->where([
            'user_id' => $user_id,
            'code' => $code,
        ]);
    }

    public function scopeActive($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subMinute());
    }
}
