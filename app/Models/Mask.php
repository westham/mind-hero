<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Mask extends Model
{
    protected $fillable = ['name', 'file', 'y_offset'];

}
