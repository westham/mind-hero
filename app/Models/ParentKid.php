<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ParentKid extends Model
{
    protected $fillable = ['parent_id', 'kid_id', 'status'];

    protected static $kids = [];

    protected static $parents = [];

    public function kid(){
        return $this->belongsTo(User::class, 'kid_id', 'id');
    }

    public function parent(){
        return $this->belongsTo(User::class, 'parent_id', 'id');
    }

    public static function kids()
    {
        $parent_kids = self::where('parent_id', Auth::user()->id)->get();

        foreach ($parent_kids as $parent_kid) {
            self::$kids[] = $parent_kid->kid()->first();
        }

        return self::$kids;
    }

    public static function parents()
    {
        $parent_kids = self::where('parent_id', Auth::user()->id)->get();

        foreach ($parent_kids as $parent_kid) {
            self::$parents[] = $parent_kid->parent()->first();
        }

        return self::$parents;
    }
}
