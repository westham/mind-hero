<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $fillable = ['email', 'key'];

    protected $hidden = ['key'];

    /** SCOPES **/

    public function scopeCheckByEmail($query, $email)
    {
        return $query->where('email', $email)->exists();
    }

    public function scopeCheckByEmailAndKey($query, $email, $key)
    {
        return $query->where([['email', $email], ['key', $key]])->exists();
    }

    public function scopeDeleteByEmail($query, $email)
    {
        return $query->where('email', $email)->delete();
    }

    public function scopeDeletePassed1H($query){
        return $query->where('created_at', '<=', Carbon::now()->subMinute()->toDateTimeString())->delete();
    }

    /** SCOPES **/
}
