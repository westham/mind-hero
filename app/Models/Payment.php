<?php

namespace App\Models;

use App\Http\Traits\FormatResponse;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Payment extends Model
{
    use FormatResponse;

    protected $fillable = ['currency', 'description', 'price', 'priceAsDecimal', 'product_id', 'title', 'user_id', 'status', 'number_kids', 'tariff_id'];


    /** RELATIONS **/

    public function kids()
    {
        return $this->belongsToMany(User::class, 'kid_payments', 'payment_id', 'kid_id')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }

    /** RELATIONS **/


    /** ACCESSORS **/

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    /** ACCESSORS **/


    public function createPayment($user, $param){
        $kids = $param['kid_id'];
        $tariff = Tariff::find($param['tariff_id']);
        foreach ($kids as $kid){
            if(!$user->kids->contains($kid)){
                $response = $this->formatResponse('error', "Kid id does not contain children, or not your own children.");
                return response($response, 403);
            }
            if(User::checkIfKidPayment($kid)){
                $response = $this->formatResponse('error', "This kids are already subscribed.");
                return response($response, 403);
            }
        }
        if(count($kids) !== $tariff->number_kids){
            $response = $this->formatResponse('error', "This payment is not similar with amount of your kids.");
            return response($response, 403);
        }
        try{
            $payment = Payment::create(
                [
                    'priceAsDecimal' => $param['priceAsDecimal'],
                    'currency' => $param['currency'],
                    'title' => $param['title'],
                    'price' => $param['price'],
                    'description' => $param['description'],
                    'user_id' => $user->id,
                    'status' => true,
                    'tariff_id' => $param['tariff_id']
                ]);

            collect($kids)->each(function ($item) use($payment, $user){
                $kid = User::find($item);
                $payment->kids()->attach($kid->id);
                $kid->access = true;
                $kid->payment = true;
                $kid->save();
            });
        } catch (\Exception $e){
            return $e->getMessage();
        }
        $response = $this->formatResponse('success', null, $payment);
        return response($response, 200);
    }


    /** SCOPES **/

    public function scopeGetPassedPeriod($query, $period){
        return $query->where('updated_at', '<', Carbon::now()->subDays($period)->toDateTimeString())->exists();
    }

    /** SCOPES **/
}
