<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    protected $fillable = ['name', 'description'];



    /** ACCESSORS **/

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    /** ACCESSORS **/


    /** RELATIONS **/

    public function consultants()
    {
        return $this->belongsToMany(User::class, 'reports', 'reason_id', 'consultant_id')->withTimestamps();
    }

    /** RELATIONS **/


}
