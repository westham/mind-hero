<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['kid_id', 'consultant_id', 'reason_id'];


    protected $appends = ['kid_name', 'consultant_name', 'reason'];
    /** RELATIONS **/

    public function kid(){
        return $this->belongsTo(User::class);
    }

    /** RELATIONS **/


    /** ACCESSORS **/

    public function getKidNameAttribute()
    {
        return User::find($this->kid_id)->name;
    }

    public function getConsultantNameAttribute()
    {
        return User::find($this->consultant_id)->name;
    }

    public function getReasonAttribute()
    {
        return Reason::find($this->reason_id)->name;
    }

    /** ACCESSORS **/


    public function send($user, $data){
        try{
            $report = new Report();
            $report->kid_id = $user->id;
            $report->consultant_id = $data['consultant_id'];
            $report->reason_id = $data['reason_id'];
            $report->save();
        } catch(\Exception $e){
            return $e->getMessage();
        }
        return $report;
    }

}
