<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['time_from', 'time_to', 'status', 'visible'];

    /** RELATIONS **/

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /** RELATIONS **/

    /** SCOPES **/

    public function scopeTrueStatus($query, $ids)
    {
        return $query->whereIn('id', $ids)->where('status', true)->pluck('id')->toArray();
    }

    public function scopeNotVisible($query, $ids)
    {
        return $query->whereIn('id', $ids)->where('visible', false)->pluck('id')->toArray();
    }

    public function scopeCurrentWeek($query)
    {
        return $query->whereBetween('time_from',
            [Carbon::now()->startOfWeek()->subDay(), Carbon::now()->endOfWeek()->subDay()
            ])->orderBy('time_from', 'asc')->get();
    }

    public function scopeNextWeek($query)
    {
        return $query->whereBetween('time_from', [
            Carbon::now()->endOfWeek()->subDay(), Carbon::now()->endOfWeek()->addWeek()->subDay()
        ])->orderBy('time_from', 'asc')->get();
    }

    /** SCOPES **/

    /** ACCESSORS **/

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getTimeFromAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
    }

    public function getTimeToAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('c');
    }

    /** ACCESSORS **/

    /** MUTATORS **/

    public function setTimeFromAttribute($date)
    {
        $this->attributes['time_from'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    public function setTimeToAttribute($date)
    {
        $this->attributes['time_to'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /** MUTATORS **/

    public function createSchedule($user, $data){
        try{
            $schedule = new Schedule();
            $schedule->time_from = $data['time_from'];
            $schedule->time_to = $data['time_to'];
            $schedule->status = false;
            $schedule->visible = false;
            $schedule->user()->associate($user);
            $schedule->save();
        } catch(\Exception $e){
            return $e->getMessage();
        }
        return $schedule;
    }

    public function updateSchedule($data){
        try{
            $this->time_from = $data['time_from'];
            $this->time_to = $data['time_to'];
            $this->save();
        } catch(\Exception $e){
            return $e->getMessage();
        }
        return $this;
    }
}
