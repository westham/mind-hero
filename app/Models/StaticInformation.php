<?php

namespace App\Models;

use App\Traits\ImageTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class StaticInformation extends Model
{
    use ImageTrait;

    protected $fillable = ['title', 'content', 'type', 'image'];


    /** ACCESSORS **/

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    /** ACCESSORS **/


    public function createStaticInfo($data){
        try {
            $staticInfo = new StaticInformation();
            $staticInfo->title = $data['title'];
            $staticInfo->content = $data['content'];
            $staticInfo->type = $data['type'];
            if(array_key_exists('image', $data)){
                $savePath = Config::get('constants.image_folder.image.save_path');
                $staticInfo->image = $this->saveImage($data['image'], $savePath);
            }
            $staticInfo->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $staticInfo;
    }

    public function updateStaticInfo($data){
        try {
            $staticInfo = StaticInformation::find($data['static_info_id']);
            $staticInfo->title = $data['title'];
            $staticInfo->content = $data['content'];
            $staticInfo->type = $data['type'];
            if(array_key_exists('image', $data)){
                $savePath = Config::get('constants.image_folder.image.save_path');
                if($staticInfo->image)
                    $this->deleteImage($staticInfo->image, $savePath);
                $staticInfo->image = $this->saveImage($data['image'], $savePath);
            }
            $staticInfo->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $staticInfo;
    }
}
