<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $fillable = ['type', 'add_info', 'user_id'];

    /** RELATIONS **/

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /** RELATIONS **/

    /** ACCESSORS **/

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;
    }

    /** ACCESSORS **/

    public function createStatistic($user, $data){
        $add_info['interlocutor_id'] = $data['add_info']['interlocutor_id'];
        try{
            $statistic = new Statistics();
            $statistic->type = $data['type'];
            if($data['type'] == 'call'){
                $add_info['time'] = $data['add_info']['time'];
            }
            $statistic->add_info = json_encode($add_info);
            $statistic->user()->associate($user);
            $statistic->save();
        } catch(\Exception $e){
            return $e->getMessage();
        }
        return $statistic;
    }

    /** SCOPES **/

    public function scopeCheck($query, $type, $interlocutorId)
    {
        return $query->where([[
            'type', $type],[
            'add_info->interlocutor_id', $interlocutorId
        ]])->exists();
    }

    public function scopeGetChatToday($query)
    {
        return $query->where([['created_at', '>=', Carbon::now()->subDays(1)->toDateTimeString()],['type', 'chat']])->get();
    }

    public function scopeGetChatWeekly($query)
    {
        return $query->where([['created_at', '>=', Carbon::now()->subDays(7)->toDateTimeString()],['type', 'chat']])->get();
    }

    public function scopeGetCallToday($query)
    {
        return $query->where([['created_at', '>=', Carbon::now()->subDays(1)->toDateTimeString()],['type', 'call']])->get();
    }

    public function scopeGetCallWeekly($query)
    {
        return $query->where([['created_at', '>=', Carbon::now()->subDays(7)->toDateTimeString()],['type', 'call']])->get();
    }

    /** SCOPES **/
}
