<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $fillable = ['productId', 'number_kids', 'period'];

    /** RELATIONS **/

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    /** RELATIONS **/
}
