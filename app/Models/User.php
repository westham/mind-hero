<?php

namespace App\Models;

use Aloha\Twilio\Twilio;
use App\Http\Traits\FormatResponse;
use Config;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Mpociot\Firebase\SyncsWithFirebase;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, FormatResponse, SyncsWithFirebase, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'phone', 'password', 'birth_date', 'grade', 'role_id', 'id_number', 'score', 'discount', 'logs', 'recommendations', 'messages', 'access', 'payment', 'register'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $casts = [
        'phone' => 'array'
    ];


    /** SCOPES **/

    public function scopeGetByPhone($query, $phone, $code)
    {
        return $query->where([
            'phone->phone' => $phone,
            'phone->code' => $code,
        ]);
    }

    public function scopeGetByEmail($query, $email)
    {
        return $query->where('email', $email);
    }

    public function scopeCheckIfKid($query, $id)
    {
        return $query->whereIn('id', $id)->where('role_id', '1')->count();
    }

    public function scopeCheckIfKidPayment($query, $id)
    {
        return $query->where([['id', $id],['payment', true]])->exists();
    }

    public function scopeGetPaidKids($query)
    {
        return $query->whereHas('role', function ($query){
            $query->where('name', 'Kid');
        })->whereHas('payments', function ($query){
            $query->where('status', true);
        })->get();
    }

    public function scopeRole($query, $role)
    {
        return $query->where([
            'role_id' => config('constants.roles.'.$role)
        ]);
    }

    /** SCOPES **/


    /** RELATIONS **/

    public function loginPhones()
    {
        return $this->hasMany(LoginPhone::class);
    }

    public function reasons()
    {
        return $this->belongsToMany(Reason::class, 'reports', 'consultant_id', 'reason_id')->withTimestamps();
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function kids()
    {
        return $this->belongsToMany(User::class, 'parent_kids', 'parent_id', 'kid_id')->withTimestamps();
    }

    public function parents()
    {
        return $this->belongsToMany(User::class, 'parent_kids', 'kid_id', 'parent_id')->withTimestamps();
    }

    public function consultantLogs()
    {
        return $this->hasMany(Log::class, 'consultant_id', 'id');
    }

    public function kidLogs()
    {
        return $this->hasMany(Log::class, 'kid_id', 'id');
    }

    public function contents()
    {
        return $this->belongsToMany(Content::class)->withTimestamps();
    }

    public function device()
    {
        return $this->hasOne(Device::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function statistics()
    {
        return $this->hasMany(Statistics::class);
    }

    public function payments()
    {
        return $this->belongsToMany(Payment::class, 'kid_payments', 'kid_id', 'payment_id')->withTimestamps();
    }

    public function paymentsP()
    {
        return $this->hasMany(Payment::class);
    }
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /** RELATIONS **/

    public function phone()
    {
        return $this->phone['code'].$this->phone['phone'];
    }

    public function getToken()
    {
        $access_token = $this->createToken('Mind-Hero')->accessToken;

        $data = [
            'user' => $this,
            'token' => $access_token
        ];

        return $data;
    }

    /*** MUTATORS ***/

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    /*** MUTATORS ***/


    /** EVENTS **/

    protected static function boot() {
        parent::boot();
        static::deleting(function($user) {
            $user->parents()->detach();
        });
    }

    /** EVENTS **/

    public function hasAnyRole($roles){
        if(is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role){
        if($this->role()->where('name', $role)->first()){
            return true;
        }
        return false;
    }
}
