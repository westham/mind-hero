<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function updateKid(User $user, User $kid)
    {
        if ($user->kids->contains($kid->id)) {
            if($user->kids()->wherePivot('status', '=', 'owner')->first() !== null) {
                return true;
            }
        }
        return false;
    }

    public function deleteKid(User $user, User $kid)
    {
        if ($user->kids->contains($kid->id)) {
            if($user->kids()->wherePivot('status', '=', 'owner')->first() !== null) {
                return true;
            }
        }
        return false;
    }

    public function getFollowers(User $user, User $kid)
    {
        if ($user->kids->contains($kid->id)) {
            if($user->kids()->wherePivot('status', '=', 'owner')->first() !== null) {
                return true;
            }
        }
        return false;
    }


}
