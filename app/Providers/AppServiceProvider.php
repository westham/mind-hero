<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('notMySelf', 'App\Http\Rules\NotMySelfRule@notMySelf');
        Validator::extend('kid', 'App\Http\Rules\KidRule@kid');
        Validator::extend('consultant', 'App\Http\Rules\ConsultantRule@consultant');
        Validator::extend('follower', 'App\Http\Rules\FollowerRule@follower');
        Validator::extend('visible', 'App\Http\Rules\VisibleRule@visible');
        Validator::extend('available', 'App\Http\Rules\AvailableRule@available');
        Validator::extend('my_content', 'App\Http\Rules\MyContentRule@myContent');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
