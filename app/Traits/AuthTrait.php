<?php
namespace App\Http\Traits;

use Twilio;
use App\Models\LoginPhone;
use App\Models\User;
use Auth;

trait AuthTrait
{
    use FormatResponse;

    public function loginPhone($phone, $code, $verification_code, $token_device)
    {
        $user = User::getByPhone($phone, $code)->first();
        if ($user == null) {
            return $this->formatResponse('error', 'User not found');
        }

        $login_request = LoginPhone::existCode($user->id, $verification_code)->active()->first();

        if ($login_request != null) {
            if ($user->device !== null) {
                $user->device()->update(['token' => $token_device]);
            } else {
                $user->device()->create(['token' => $token_device]);
            }

            Auth::login($user);
            $data = $user->getToken();
            $login_request->delete();
            return $this->formatResponse('success', null, $data);
        } else {
            return $this->formatResponse('error', 'Code not found');
        }
    }

    public function loginEmail($login, $password, $type_login = 'email')
    {
        if(Auth::attempt([$type_login => $login, 'password' => $password])){
            $user = Auth::user();

            $data = $user->getToken();

            return $this->formatResponse('success', null, $data);
        } else {
            return $this->formatResponse('error', 'Login or password is wrong');
        }
    }

    public function sendCode($request)
    {
        $user = User::getByPhone($request->phone, $request->code)->first();

        if ($user == null) {
            $user = User::create(
                [
                    'phone' => ['phone' => $request->phone, 'code' => $request->code],
                    'role_id' => 2,
                ]);
        } else {
            $old_request = LoginPhone::where('user_id', $user->id)->first();

            if ($old_request !== null) {
                $old_request->delete();
            }
        }

        $code = rand(1000, 9999);

        LoginPhone::create(['code' => $code, 'user_id' => $user->id]);

//        return $this->formatResponse('success', null, $code);

//        Twilio::message($user->phone(), 'Your verification code is '. $code);

        return $this->formatResponse('success', null, $code);
    }


}