<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 21.08.2018
 * Time: 19:20
 */

namespace App\Traits;


use App;
use App\Mail\DemoMail;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

trait EmailTrait
{
    public function sendMail($user){
        $key = $this->generateKey($user);
        $link = Config::get('constants.url_for_mail').Config::get('constants.links.reset_password').'?key='.$key;
        Mail::to($user->email)->send(new DemoMail($user, $link));
    }

    private function generateKey($user){
        $salt = str_random(8);
        $key = bcrypt($user->email.time().$salt);
        PasswordReset::create(['email' => $user->email, 'key' => $key]);
        return $key;
    }
}