<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 15.08.2018
 * Time: 17:22
 */

namespace App\Traits;


use Illuminate\Support\Facades\Storage;

trait ImageTrait
{
    public function saveImage($image, $path){
        $fileName = 'image_'.time().'_'.str_random(10).'.png';
        $image->storeAs($path, $fileName);
        return $fileName;
    }

    public function deleteImage($name, $path){
        Storage::disk('local')->delete($path.$name);
    }
}