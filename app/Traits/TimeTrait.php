<?php
/**
 * Created by PhpStorm.
 * User: nchvn
 * Date: 28.08.2018
 * Time: 16:38
 */

namespace App\Traits;


trait TimeTrait
{
    public function sumTime($times){
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }
}