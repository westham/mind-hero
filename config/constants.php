<?php

 return [
     'roles' => [
         'kid' => 1,
         'parent' => 2,
         'consultant' => 3,
         'manager' => 4,
     ],
     'limit_content' => 6,
     'image_folder' => [
         'image' => [
             'save_path' => 'public/images/',
             'get_path' => 'api/storage/images/',
         ],
         'masks' => [
             'save_path' => 'public/masks/',
             'get_path' => 'api/storage/masks/',
             'resize_path' => storage_path().'/app/public/masks/'
         ],
     ],
     'links' => [
         'reset_password' => '#/authorization/new-password'
     ],
     'url_for_mail' => 'https://mind-hero.grassbusinesslabs.tk/'
 ];