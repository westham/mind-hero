<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->json('phone')->nullable();
            $table->bigInteger('id_number')->unique()->nullable();

            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();

            $table->string('birth_date')->nullable();
            $table->string('grade')->nullable();
            $table->string('score')->nullable();
            $table->integer('discount')->nullable();
            $table->string('status')->nullable()->default('normal');
            $table->integer('access')->default(0);
            $table->integer('payment')->default(0);
            $table->integer('register')->nullable()->default(0);
            $table->integer('logs')->nullable();
            $table->integer('recommendations')->nullable();
            $table->integer('messages')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
