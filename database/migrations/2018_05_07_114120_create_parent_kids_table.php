<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentKidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_kids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id', false, 10);
            $table->integer('kid_id', false, 10);
            $table->string('status')->nullable();

            $table->foreign('parent_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('kid_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_kids');
    }
}
