<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeImageColumnInStaticinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('static_informations', function($table) {
            $table->dropColumn('image');
        });
        Schema::table('static_informations', function($table) {
            $table->string('image')->after('type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('static_informations', function($table) {
            $table->dropColumn('image');
        });
    }
}
