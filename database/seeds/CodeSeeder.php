<?php

use Illuminate\Database\Seeder;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $response = Unirest\Request::get("https://restcountries-v1.p.mashape.com/all",
            array(
                "X-Mashape-Key" => env('COUNTRY_CODES', 'PXWylAyM0emshkRfww3ImCmI5lIAp1rzANajsnqokriYSdN0kU'),
                "Accept" => "application/json"
            )
        );

        foreach ($response->body as $coutry) {
            foreach ($coutry->callingCodes as $callingCode) {
                if ($callingCode !== '') {
                    DB::table('country_codes')->insert([
                        'name' => $coutry->name,
                        'code' => '+'.$callingCode,
                    ]);
                }
            }
        }
    }
}
