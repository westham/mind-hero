<?php

use Illuminate\Database\Seeder;

class TariffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tariffs')->insert([
            'productId' => 'subscribe.month.1',
            'period' => 30,
            'number_kids' => 1,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('tariffs')->insert([
            'productId' => 'subscribe.month.2',
            'period' => 30,
            'number_kids' => 2,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('tariffs')->insert([
            'productId' => 'subscribe.month.3',
            'period' => 30,
            'number_kids' => 3,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('tariffs')->insert([
            'productId' => 'subscribe.month.4',
            'period' => 30,
            'number_kids' => 4,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('tariffs')->insert([
            'productId' => 'subscribe.month.5',
            'period' => 30,
            'number_kids' => 5,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('tariffs')->insert([
            'productId' => 'subscribe.month.6',
            'period' => 30,
            'number_kids' => 6,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
