<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Kid',
            'email' => 'kid@gmail.com',
            'role_id' => 1,
            'grade' => '2B',
            'phone' => '{"code": "+972", "phone": "06816626901"}',
            'id_number' => 11111,
            'birth_date' => '1970-01-01 00:00:00',
        ]);

        DB::table('users')->insert([
            'name' => 'Parent',
            'email' => 'parent@gmail.com',
            'role_id' => 2,
            'phone' => '{"code": "+972", "phone": "06816626902"}',
            'id_number' => 22222,
            'birth_date' => '1970-01-01 00:00:00',
        ]);

        DB::table('users')->insert([
            'name' => 'Consultant',
            'email' => 'consultant@gmail.com',
            'password' => bcrypt('12345678'),
            'role_id' => 3,
            'phone' => '{"code": "+972", "phone": "06816626903"}',
            'id_number' => 33333,
            'birth_date' => '1970-01-01 00:00:00',
        ]);

        DB::table('users')->insert([
            'email' => 'admin@admin.admin',
            'password' => bcrypt('Test1234!'),
            'role_id' => 5,
        ]);
    }
}
