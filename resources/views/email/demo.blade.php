<!DOCTYPE html>
<html>
<head>
    <title>Reseting password</title>
</head>

<body>
<h2>Hi {{$user->name}},</h2>
<br/>
We got a request to reset your Mind-Hero password.
<br/>
<a href="{{$link}}" class="button">Go to reset</a>
<br/>
If you ignore this message, your password wont't be changed.
</body>

</html>

