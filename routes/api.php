<?php

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/test', function ()
{
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/../firebase-config.json');

    $firebase = (new Factory)

        ->withServiceAccount($serviceAccount)

        ->withDatabaseUri('https://mind-hero-96b57.firebaseio.com/')

        ->create();

    $database = $firebase->getDatabase();

    $notification_groups = $database->getReference('logs')
            ->getValue();

    dd($notification_groups);
//
//    foreach ($notification_groups as $notification_group) {
//        if ($notification_group !== null) {
//            foreach ($notification_group as $notification) {
//                dd($notification);
//            }
//        }
//    }
//
//    return $notifications;




//$newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-

//$newPost->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-

//$newPost->getChild('title')->set('Changed post title');

//$newPost->getValue(); // Fetches the data from the realtime database

//$newPost->remove();

//    echo"<pre>";

//    print_r($newPost->getvalue());

});


Route::get('/list-codes', 'AuthController@listPhoneCodes');

/** PASSWORD */
Route::post('password/forgot', 'PasswordController@forgot');
Route::post('password/reset', 'PasswordController@reset');


Route::group(['middleware' => 'login-access-for-kid'], function () {

    /** LOGIN */
    Route::post('/login', 'AuthController@login');
    Route::post('/check-phone', 'AuthController@checkPhone');

});

/** CONSULTANT */
/** list contents for main page */
Route::get('/list-consultants', 'ConsultantController@index');

Route::group(['middleware' => ['auth:api', 'access']], function () {
    /** USER */
    /** get current user */
    Route::get('/get-user', 'UserController@show');
    /** update current user */
    Route::post('/user-update', 'UserController@update');
    /** get all */
    Route::get('users', 'UserController@getAll');

    /** logout */
    Route::get('/logout', 'AuthController@logout');

    /** PARENT */
    /** list followers for kid */
    Route::post('/list-followers', 'ParentController@listFollowers');
    /** check status kid for parent */
    Route::post('/check-status-parent', 'ParentController@checkStatusParent');
    /** add new follower (parent) to kid */
    Route::post('/add-another-parent', 'ParentController@addAnotherParent');
    /** delete follower (parent) */
    Route::post('/delete-another-parent', 'ParentController@deleteAnotherParent');
    /** list parents for kid */
    Route::post('/list-parents', 'ParentController@listParents');
    /** get all parents */
    Route::get('parents', 'ParentController@getAll');


    /** KID */
    /** list kids for parent */
    Route::get('/list-kids', 'KidController@index');
    /** create kid */
    Route::post('/create-kid', 'KidController@create');
    /** update current user */
    Route::post('/update-kid', 'KidController@update');
    /** delete kid */
    Route::post('/delete-kid', 'KidController@delete');
    /** change access for kid */
    Route::post('/access-kid', 'KidController@changeAccess');
    /** get all parents */
    Route::get('kids', 'KidController@getAll');

    /** CONSULTANT */
    /** create consultant */
    Route::post('/create-consultant', 'ConsultantController@create');


    /** LOG */
    /** list kids for parent */
    Route::post('/list-logs-kid', 'LogController@index');
    /** create log */
    Route::post('/create-log', 'LogController@create');
    /** delete log */
    Route::post('/delete-log', 'LogController@delete');
    /** emergency log */
    Route::get('/emergency-logs', 'LogController@emergencyLogs');

    /** CONTENT */
    /** list contents for main page */
    Route::get('/list-contents', 'ContentController@index');
    /** list live contents */
    Route::get('/live-contents', 'ContentController@liveContents');
    /** list contents for content page */
    Route::get('/get-contents', 'ContentController@contentsList');
    /** favourites list */
    Route::get('/favourites-list', 'ContentController@favouritesList');
    /** search content */
    Route::post('/search-content', 'ContentController@search');
    /** add content to favourites */
    Route::post('/add-to-favourites', 'ContentController@addToFavourites');


    /** STATIC INFORMATION */
    /** list static information */
    Route::post('/list-static-information', 'StaticInformationController@index');
    /** create static information */
    Route::post('/create-static-information', 'StaticInformationController@create');
    /** update static information */
    Route::post('/update-static-information', 'StaticInformationController@update');
    /** delete static information */
    Route::post('/delete-static-information', 'StaticInformationController@delete');

    /** TARIFF */
    /** list tariff */
    Route::post('/list-tariff', 'TariffController@index');
    /** create tariff */
    Route::post('/create-tariff', 'TariffController@create');
    /** update tariff */
    Route::post('/update-tariff', 'TariffController@update');
    /** delete tariff */
    Route::post('/delete-tariff', 'TariffController@delete');

    /** PAYMENT */
    /** list payments */
    Route::get('/list-payment', 'PaymentController@index');
    /** create payment */
    Route::post('/create-payment', 'PaymentController@create');

    /** SERVER */
    /** get all */
    Route::get('servers', 'ServerController@getAll');
    /** create */
    Route::post('server/create', 'ServerController@create');
    /** update */
    Route::post('server/update', 'ServerController@update');


    Route::group(['middleware' => 'roles', 'roles' => ['Consultant', 'Admin']], function (){

        /** SCHEDULE */
        /** create schedule */
        Route::post('schedule/create', 'ScheduleController@create');
        /** create schedule */
        Route::post('schedule/update', 'ScheduleController@update');
        /** make visible schedule */
        Route::post('schedule/visible', 'ScheduleController@visible');
        /** get my all schedules */
        Route::get('schedules/my', 'ScheduleController@getMy');

        /** STATISCTICS */
        /** add statistics */
        Route::post('statistics/add', 'StatisticsController@add');
        /** get chat today statistics */
        Route::get('statistics/my/chat/today', 'StatisticsController@getMyChatTodayStatistic');
        /** get call today statistics */
        Route::get('statistics/my/call/today', 'StatisticsController@getMyCallTodayStatistic');
        /** get weekly statistics */
        Route::get('statistics/my/weekly', 'StatisticsController@getMyWeeklyStatistic');

        /** CONTENT */
        Route::get('content/my', 'ContentController@getMy');
        /** create content */
        Route::post('/create-content', 'ContentController@create');
        /** update content */
        Route::post('/update-content', 'ContentController@update');
        /** remove content from favourites */
        Route::post('/remove-from-favourites', 'ContentController@removeFromFavourites');
        /** delete content */
        Route::post('/delete-content', 'ContentController@delete');

    });

    Route::group(['middleware' => 'roles', 'roles' => ['Manager', 'Admin']], function (){

        /** SCHEDULE */
        /** get by consultant */
        Route::get('schedules/{consultant}', 'ScheduleController@getByConsultant');
        /** get all schedules */
        Route::get('schedules', 'ScheduleController@getAll');
        /** approve schedules */
        Route::post('schedule/approve', 'ScheduleController@approve');
    });

    Route::group(['middleware' => 'roles', 'roles' => ['Consultant', 'Manager', 'Admin']], function (){
        /** SCHEDULE */
        /** delete schedule */
        Route::post('schedule/delete', 'ScheduleController@delete');
        /** get my 2 weeks schedules */
        Route::get('schedules/my/weeks', 'ScheduleController@getMy2Weeks');
    });

    Route::group(['middleware' => 'roles', 'roles' => ['Kid']], function (){
        /** REPORTS */
        Route::post('report/send','ReportController@sendReport');
    });

    Route::group(['middleware' => 'roles', 'roles' => ['Admin']], function (){
        /** CONSULTANT */
        Route::post('consultant/update','ConsultantController@update');
        /** delete consultants */
        Route::delete('consultant', 'ConsultantController@delete');
        /** get all 2 weeks schedules */
        Route::get('schedules/all/weeks', 'ScheduleController@getAll2Weeks');
    });

    /** KID */
    /** list paid kids */
    Route::get('/list-paid-kids', 'KidController@getPaidKids');

    /** REASONS */
    Route::post('reason/create','ReasonController@store');
    Route::get('reasons','ReasonController@index');
    Route::post('reason/update','ReasonController@update');
    Route::post('reason/delete','ReasonController@delete');

    /** MASKS */
    Route::post('mask/create','MaskController@create');
    Route::post('mask/update','MaskController@update');
    Route::post('mask/delete','MaskController@delete');
    Route::get('masks','MaskController@getAll');

});